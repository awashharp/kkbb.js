/**
 *  kkbb.js
 *
 *  jQuery plugin template reference: http://css-tricks.com/snippets/jquery/jquery-plugin-template/
 */
/*jslint nomen: true, sloppy: true, vars: true, white: true, browser: true */

(function ($) {

  // HTML Templates
  var barTempl = '<div class="kkbb_b"><div class="kkbb_h"></div><div class="kkbb_f"></div></div>';

  // Class object, initalized one instance for each installed element
  // The base is the element of this newly created instance
  $.kkbb = function(base, elm, options) {

    // Reference of binding element 
    base.$elm = $(elm);
    base.elm = elm;
    // Status of bar UI activation
    base.active = false;
    // Real password accepted by plugin
    base.password = "";

    // Internal timer and direction status
    base._timer = 0;
    base._direction = 0; // 0 for forward, 1 for backward

    // Save this newly create object into DOM object
    base.$elm.data("kkbb", base);

    /** Initialization **/
    var opts = null;  // Options vairable shorthand
    var $bg = null;   // Background Area
    var $ha = null;   // Hit Area
    var $fg = null;   // Foreground Area
    var barWidth = 0; // Bar width related to password input field

    // Options initialization
    if(typeof(options) === "object") {
      base.options = $.extend({}, $.kkbb.defaults, options);
    } else {
      base.options = $.extend({}, $.kkbb.defaults);
    }
    opts = base.options;

    // Bar creation and insertion
    base.$bar = $(barTempl);
    $bg = base.$bar;                  
    $ha = base.$bar.find('.kkbb_h');  
    $fg = base.$bar.find('.kkbb_f');  
    barWidth = parseInt(base.$elm.outerWidth(), 10);
    $bg.css({
      "width"       : barWidth - 2 + "px",
      "height"      : opts.elementHeight + "px",
      "border"      : "1px solid " + opts.backgroundBorderColor,
      "borderRadius": opts.elementHeight / 2 + "px",
      "background"  : opts.backgroundColor,
      "position"    : "relative"
    });
    $ha.css({
      "width"       : barWidth * opts.hitAreaPercentage,
      "height"      : opts.elementHeight + "px",
      "borderRadius": opts.elementHeight / 2 + "px",
      "background"  : opts.hitAreaColor,
      "position"    : "absolute",
      "top"         : "0",
      "left"        : barWidth * (1 - opts.hitAreaPercentage) / 2
    });
    $fg.css({
      "width"       : opts.elementHeight + "px",
      "height"      : opts.elementHeight + "px",
      "borderRadius": opts.elementHeight / 2 + "px",
      "background"  : opts.foregroundColor,
      "position"    : "absolute",
      "top"         : "0",
      "left"        : "0"
    });
    base.$elm.after(base.$bar);

    // Bar animation
    base._timer = setInterval(function () {
      var width = parseInt($bg.css("width"), 10);
      var min = 0;
      var max = width - parseInt($fg.css("width"), 10);
      var pos = parseInt($fg.css("left"), 10);
      var inc = width / (opts.roundTripMilliseconds / 20);

      if(pos >= max)      { base._direction = 1; }
      else if(pos <= min) { base._direction = 0; }

      if(base._direction === 0)       { $fg.css("left", "+=" + inc + "px"); }
      else if(base._direction === 1)  { $fg.css("left", "-=" + inc + "px"); }

      // Validate the position of foreground
      base._validate(
        parseInt($fg.css("left"), 10),
        parseInt($ha.css("left"), 10),
        parseInt($ha.css("left"), 10) + parseInt($ha.width(), 10) - parseInt($fg.width(), 10)
        );
    }, 10);

    // Validate the status of bar UI activation
    base._validate = function (pos, boundLeft, boundRight) {
      var old = base.active;
      if(pos >= boundLeft && pos <= boundRight) {
        base.active = true;
      } else {
        base.active = false;
      }

      // Generate events only at edge
      if(old !== base.active && old === false && base.active === true) {
        base.$elm.trigger("active");
      } else if(old !== base.active && old === true && base.active === false) {
        base.$elm.trigger("inactive");
      }
    };

    // Give status of foreground, returnig direction information and position percentage
    base.getStatus = function () {
      var result = {
        direction: 0,
        position: 0
      };
      var max = parseInt($bg.css("width"), 10) - parseInt($fg.css("width"), 10);
      var left = parseInt($fg.css("left"), 10);
      result.direction = base._direction;
      result.position = left * 100 / max;
      return result;
    };

    // Bind on UI change active/inactive events
    base.$elm.on("active", function () {
      $fg.css("background", opts.foregroundActiveColor);
    });
    base.$elm.on("inactive", function () {
      $fg.css("background", opts.foregroundColor);
    });

    // Add event for keystrokes listening
    // Notice that this listens to "keypress" event, not "keydown" or "keyup"
    // For more key events details, please refer to http://unixpapa.com/js/key.html
    base.$elm.on("keypress", function (e) {
      var asciiCode = e.keyCode || e.which;
      var c = String.fromCharCode(asciiCode);

      // Accept alphabet and number only
      if(/[A-Za-z0-9]/.test(c) && base.active) {
        base.password += c;
        // Send a success event to indicate user that a keystroke is accepted
        base.$elm.trigger("success", base.password);
      }
    });

    // Since "keypress" cannot detect backspace key, we need extra event listener
    base.$elm.on("keydown", function (e) {
      var key = e.keyCode || e.which;

      // Backspace support
      if(key === 8) {
        base.password = base.password.slice(0, -1);
        // Send a success event to indicate user that a keystroke is accepted
        base.$elm.trigger("success", base.password);
      }
    });

    // Interception to change password input field when option is on
    if(opts.autobindOnSubmit === true) {
      base.$elm.closest("form").on("submit", function () {
        base.$elm.val(base.password);
      });
    }
  };

  // All possible options
  $.kkbb.defaults = {
    // Bar color appearance
    "backgroundColor"       : "#EAEAEA",
    "backgroundBorderColor" : "#CCCCCC",
    "hitAreaColor"          : "#CCCCCC",
    "foregroundColor"       : "#333333",
    "foregroundActiveColor" : "#339900",
    // Bar general appearance
    "elementHeight"         : 18,         // Height of the progressbar, in pixel
    "hitAreaPercentage"     : 0.5,        // Percentage from 0 to 1 related to width of progressbar
    // Animation
    "roundTripMilliseconds" : 5000,       // Milliseconds for a round trip time of foreground
    // Miscellaneous
    "autobindOnSubmit"      : true        // When true, the value of password would be change into
                                          // stored password.
                                          // If false, you should handle it yourself
  };

  // Add functionality into jQuery object prototype
  $.fn.kkbb = function (options) {

    var install = function (elm, options) {
      // Only install it when kkbb is not currently presented
      if(!$(elm).data("kkbb") || (options !== null && typeof(options) === "object")) {
        $.kkbb({}, elm, options);
      }
    };

    return this.each(function () {
      // Only install on password field
      var clildren = $(this).find("input[type=password]");
      if(!$(this).is("input[type=password]") && clildren.length === 0) {
        return;
      }
      if(clildren.length !== 0){
        clildren.each(function () {
          install(this, options);
        });
      } else {
        install(this, options);
      }
    });
  };

}(jQuery));