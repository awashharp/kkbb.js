# kkbb.js

A keylogger obfuscation jQuery plugin, creating a obfuscation UI around the password field.

This is an unofficial implementation about Quick-Time-Event(QTE) method of keylogger obfuscation in the paper [*KKBB: Kernel Keylogger Bye-Bye*](http://opac.lib.ncu.edu.tw/record=b2196478*cht). You can get more detail in this paper.

**All intellectual property rights belongs to original paper author. Be sure to ask for approval from the paper author to use this plugin.**

This program provide "AS IS", without any warranty, so use it with care! And have fun!

# Requirement

To use this plugin, you need to download the latest [jQuery library](http://jquery.com).

# Usage

Include the `kkbb.js` file into your page after jQuery library is loaded.

Then select password `input` field you want to protect and invoke `kkbb()`:

    :::javascript
    /* Directly select the password input field */
    $('input[type=password]').kkbb();
    /* Or select the form containing a password input field */
    $('form').kkbb();

You can select a larger range, and this plugin will search out all the password field, and add UI around them.

Customizing the instance of plugin can be done simply passing an object into parameter, for example:

    :::javascript
    $('input[type=password]').kkbb({ "elementHeight": 12 });

There are a few other options described in the **Options** section.

Once `kkbb` is called, there will be a bar appended under the password field. The detail of this bar is written inside that paper.

# Functions and Internal Data Access

After initialization, you can get the handle from bound password input field:

    :::javascript
    var kkbbHandle = $('input[type=password]').data('kkbb');

There are a few data and functions you can use:

* Object: `active` - The status of the UI. If `true`, the foreground is inside hit area.

* Object: `password` - The real password stored inside this UI.

* Function: `getStatus()` - Return an object containing `direction` and `position` members.  
`direction` - the movement direction of the foreground, `0` for positive(right) movement, `1` for negative(left) movement.  
`position` - the percentage of position from the left. The value range is within 0 to 100.

You _should not_ touch other data inside this handle, those functions and data may not be accessible in the future.

# Events

There are three events that would be sent from the password field by using jQuery's `trigger()`:

* `active` - Sent when the status of UI changes into active mode.

* `inactive` - Sent when the status of UI changes into inactive mode.

* `success` - Sent when a keystroke is accepted by the UI, indicating a new word is added into password variable, or a deletion of a word.

# Options

Sticking with the default value should work quite well. But you can change some options inside this plugin.

The value inside parentheses is the default value of that option.

## Bar Color Appearance

* `backgroundColor` `("#EAEAEA")` - The background color of "Background Area".

* `backgroundBorderColor` `("#CCCCCC")` - The border color of border of "Background Area".

* `hitAreaColor` `("#CCCCCC")` - The background color of "Hit Area".

* `foregroundColor` `("#333333")` - The background color of "Foreground Area".

* `foregroundActiveColor` `("#339900")` - The background color of "Foreground Area" when it is actived(when overlapped with Hit Area).

## Bar General Appearance

* `elementHeight` `(18)` - Integer. The height of the bar in pixel, not including the border.

* `hitAreaPercentage` `(0.5)` - Real number from 0.0 to 1.0. The percentage of Hit Area related to the 

## Animation Related Options

* `roundTripMilliseconds` `(5000)` - Integer. The milliseconds foreground would spend to make a round trip of Background Area. **Don't expect the time to be accurate!**

## Miscellaneous

* `autobindOnSubmit` `(true)` - Boolean. If `true`, plugin listen to `submit` event of belonging form, and change value of password field into stored password. If `false`, you should handle the password replacement for yourself.

# Credit

Thanks to all the people in the lab for creating such a great keylogger obfucation idea. And thanks for the great food!:)

If you have any question about this program, please contact us: [Advanced Defense Lab](http://adl.csie.ncu.edu.tw)!